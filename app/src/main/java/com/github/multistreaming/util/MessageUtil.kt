package com.github.multistreaming.util

import android.util.Log
import com.sendbird.android.BaseMessage
import com.sendbird.android.UserMessageParams

object MessageUtil {
    /**
     * First-Header
     * TYPE_MANAGE : 룸 관리용
     * TYPE_CHAT : 일반 채팅용
     */
    val TYPE_MANAGE = "TYPE_MANAGE"
    val TYPE_CHAT = "TYPE_CHAT"

    /**
     * Second-Header
     * MANAGE_USER_JOIN : 룸 접속 브로드캐스트
     */
    val MANAGE_USER_JOIN = "USER_JOIN"

    /**
     * 그 외 상수
     */
    val CASTER = "CASTER"
    val NOT_CASTER = "NOT_CASTER"

    /**
     * 새로운 유저 참석
     * TYPE_MANAGE:USER_JOIN:RemonToken,Type["Caster"|"User"]
     */
    fun makeJoinMessage(remonToken : String, isCaster : String) : UserMessageParams {
        val sb = StringBuilder().run {
            append(TYPE_MANAGE)
            append(";")
            append(MANAGE_USER_JOIN)
            append(";")
            append(remonToken)
            append(",")
            append(isCaster)
        }
        return UserMessageParams().apply { setData(sb.toString())}
    }

    fun unpackMessage(message : BaseMessage, callback : AbstractMessageCallback){
        val protocol = message.data
        val splits = protocol.split(";")
        val firstHeader = splits[0]
        val secondHeader = splits[1]
        val meta = splits[2]

        when(firstHeader){
            TYPE_MANAGE -> {
                when(secondHeader){
                    MANAGE_USER_JOIN -> {
                        val detailMeta = meta.split(",")
                        val remonToken = detailMeta[0]
                        val isCaster = detailMeta[1]
                        callback.onNewUserJoined(remonToken,isCaster)
                    }
                }
            }
            TYPE_CHAT -> {

            }
        }
    }

}