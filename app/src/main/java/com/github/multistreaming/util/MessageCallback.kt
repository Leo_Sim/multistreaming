package com.github.multistreaming.util

interface MessageCallback {
    fun onNewUserJoined(remonToken : String, isCaster : String)
}