package com.github.multistreaming.chat.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.multistreaming.R
import com.sendbird.android.BaseMessage
import com.sendbird.android.UserMessage

class OpenChatAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_USER_MESSAGE = 10
    private val VIEW_TYPE_FILE_MESSAGE = 20
    private val VIEW_TYPE_ADMIN_MESSAGE = 30

    private var mMessageList = mutableListOf<BaseMessage>()


    fun setMessageList(messages: List<BaseMessage>) {
        mMessageList.addAll(messages)
        notifyDataSetChanged()
    }

    fun addFirst(message: BaseMessage) {
        Log.d("devsim","pq : " + message.data)
        mMessageList.add(0, message)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return UserMessageHolder(inflater.inflate(R.layout.list_message,parent,false))
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_USER_MESSAGE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = mMessageList[position]
        (holder as UserMessageHolder).bind(message as UserMessage)
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    private inner class UserMessageHolder : RecyclerView.ViewHolder {
        private val messageTxt : TextView
        constructor(itemView : View) : super(itemView) {
            messageTxt = itemView.findViewById(R.id.message_txt)
        }

        public fun bind(message : UserMessage){
            messageTxt.text = message.message
        }
    }
}