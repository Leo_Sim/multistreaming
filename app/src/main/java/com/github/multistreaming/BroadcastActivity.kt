package com.github.multistreaming

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.multistreaming.chat.ConnectionManager
import com.github.multistreaming.chat.adapter.OpenChatAdapter
import com.github.multistreaming.model.RoomList
import com.github.multistreaming.network.NetworkModule
import com.github.multistreaming.util.MessageUtil
import com.google.firebase.firestore.FirebaseFirestore
import com.remotemonster.sdk.Config
import com.remotemonster.sdk.RemonConference
import com.remotemonster.sdk.RemonException
import com.sendbird.android.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_broadcast.*
import org.webrtc.SurfaceViewRenderer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BroadcastActivity : AppCompatActivity(){
    private val CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_OPEN_CHAT"
    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_OPEN_CHAT"
    private var mChannelUrl: String =
        "sendbird_open_channel_741_7befa4cd8ddb194c782f937b7c4c7507a0c79bf0"
    private val CHANNEL_LIST_LIMIT = 30

    private lateinit var roomID : String
    private lateinit var mChannel: OpenChannel

    private var mConference = RemonConference()
    private var mPrevMessageListQuery: PreviousMessageListQuery? = null
    private lateinit var mChatAdapter: OpenChatAdapter
    private lateinit var mLayoutManager: LinearLayoutManager

    private var mError: RemonException? = null

    private lateinit var mSurfaceViewArray: Array<SurfaceViewRenderer>
    private lateinit var mAvailableView:Array<Boolean>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast)
        roomID = intent.getStringExtra("room")

        connectChat(roomID)
        initView()
//        initRemonConference()
    }

    private fun initView(){

        val decorView = window!!.decorView
        val options = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        decorView.systemUiVisibility = options
        decorView.setOnSystemUiVisibilityChangeListener {
            if (android.R.attr.visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                decorView.systemUiVisibility = options
            }
        }

        send_btn.setOnClickListener { sendUserMessage(msg_edit.text.toString()) }
        setUpRecyclerView()

        layout_remote1.setPosition(0,0,50,25)
        layout_remote2.setPosition(50,0,50,25)

        layout_remote3.setPosition(0,25,50,25)
        layout_remote4.setPosition(50,25,50,25)

        layout_remote5.setPosition(0,50,50,25)
        layout_remote6.setPosition(50,50,50,25)

        layout_remote7.setPosition(0,75,50,25)
        layout_remote8.setPosition(50,75,50,25)

        mSurfaceViewArray = arrayOf(
            surf_local,
            surf_remote1,
            surf_remote2,
            surf_remote3,
            surf_remote4,
            surf_remote5,
            surf_remote6,
            surf_remote7,
            surf_remote8
            )

        mAvailableView = Array(mSurfaceViewArray.size) {false}
    }

    private fun initRemonConference() {
        mError = null

        val config  = Config()
        config.context = this
        config.serviceId="42ad27d0-e50d-4137-8fa7-07e2f024a800"
        config.key="a63c2e56a50d7779f208fa8f23e37f64f8ee062843e8b194d973d55740df22ae"

        mConference.create ( roomID, config ) {
            // 마스터 유저(송출자,나자신) 초기화
            it.config.localView = mSurfaceViewArray[0]
            it.config.remoteView = null
            mAvailableView[0] = true

        }.on("onRoomCreated") {
            // 마스터 유저가 접속된 이후에 호출(실제 송출 시작)
            // TODO: 실제 유저 정보는 각 서비스에서 관리하므로, 서비스에서 채널과 실제 유저 매핑 작업 진행
            Log.d("devsim","onRoomCreated: " + it.id +"|" + mConference.me.id)
//            broadcastJoin(it.id,MessageUtil.CASTER)

        }.on("onUserJoined") {
            // 다른 사용자가 입장한 경우 초기화를 위해 호출됨
            // TODO: 실제 유저 매핑 : it.id 값으로 연결된 실제 유저를 얻습니다.
            Log.d("devsim","onUserJoined: " + it.id)
            broadcastJoin(mConference.me.id,MessageUtil.CASTER)
            // 뷰 설정
            val index = getAvailableView()
            if( index > 0 ) {
                it.config.localView = null
                it.config.remoteView = mSurfaceViewArray[index]
                it.tag = index
            }

            this.updateViews()
        }.on("onUserConnected") {
            // sdk 2.7.3 추가

        }.on("onUserLeft") {
            Log.d("devsim","onUserleft : " + it.id)
            // 다른 사용자가 퇴장한 경우
            // it.id 와 it.tag 를 참조해 어떤 사용자가 퇴장했는지 확인후 퇴장 처리를 합니다.
            val index = it.tag as Int
            mAvailableView[index] = false

            if( it.latestError != null) {
                // 에러로 인해 종료된 경우
                // 재시도나 에러 메시지 표시 등 서비스에서 구현
            }
            this.updateViews()
        }.close {
            // 마스터유저가 끊어진 경우 호출됩니다.
            // 송출이 중단되면 그룹통화에서 끊어진 것이므로, 다른 유저와의 연결도 모두 끊어집니다.
            if(mError != null ) {
                // 에러로 종료됨
            } else {
                // 종료됨
            }
            Log.d("devsim", "onClose")
        }.error {
            // 송출 채널의 오류 발생시 호출됩니다.
            // 오류로 연결이 종료되면 error -> close 순으로 호출됩니다.
            mError = it
            Log.e("devsim", "error=" + it.description)
        }
    }

    private fun connectChat(
        id : String
    ) {
        if (TextUtils.isEmpty(roomID)) {
            return
        }
        ConnectionManager.login(id, SendBird.ConnectHandler { user, e ->
            if (e != null) { // Error!
                Toast.makeText(
                    this@BroadcastActivity, "" + e.code + ": " + e.message,
                    Toast.LENGTH_SHORT
                )
                    .show()
                return@ConnectHandler
            }
            PreferenceUtils.setConnected(true)
            PreferenceUtils.setUserId(id)

            refreshFirst()
        })
    }

    override fun onResume() {
        super.onResume()
        /**
         * 메시지 리시버
         */
        SendBird.addChannelHandler(
            CHANNEL_HANDLER_ID,
            object : SendBird.ChannelHandler() {
                override fun onMessageReceived(
                    baseChannel: BaseChannel,
                    baseMessage: BaseMessage
                ) { // Add new message to view
                    if (baseChannel.url == mChannelUrl) {
                        mChatAdapter.addFirst(baseMessage)
                    }
                }
            })
    }

    private fun setUpRecyclerView() {
        mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.setReverseLayout(true)
        mChatAdapter = OpenChatAdapter()
        chat_list.setLayoutManager(mLayoutManager)
        chat_list.setAdapter(mChatAdapter)
    }

    private fun refreshFirst() {
        enterChannel(mChannelUrl)
    }

    private fun enterChannel(channelUrl: String) {
        OpenChannel.getChannel(channelUrl,
            OpenChannel.OpenChannelGetHandler { openChannel, e ->
                if (e != null) { // Error!
                    Log.d("devsim", "enterChannel err1 : " + e.message)
                    return@OpenChannelGetHandler
                }
                // Enter the channel
                openChannel.enter(OpenChannel.OpenChannelEnterHandler { e ->
                    if (e != null) { // Error!
                        Log.d("devsim", "enterChannel err2 : " + e.message)
                        return@OpenChannelEnterHandler
                    }
                    Log.d("devsim", "enterChannel success")
                    mChannel = openChannel
                    initRemonConference()
                    refresh()
                })
            })
    }

    private fun refresh() {
        loadInitialMessageList(CHANNEL_LIST_LIMIT)
    }

    private fun loadInitialMessageList(numMessages: Int) {
        mPrevMessageListQuery = mChannel.createPreviousMessageListQuery()
        mPrevMessageListQuery!!.load(
            numMessages,
            true,
            PreviousMessageListQuery.MessageListQueryResult { list, e ->
                if (e != null) { // Error!
                    e.printStackTrace()
                    return@MessageListQueryResult
                }
                mChatAdapter.setMessageList(list)
            })
    }


    override fun onBackPressed() {
        mConference.leave()
        super.onBackPressed()
    }

    override fun onPause() {
        ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID)
        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID)
        super.onPause()
    }

    override fun onDestroy() {
//        mConference.leave()
        if (mChannel != null) {
            mChannel.exit(OpenChannel.OpenChannelExitHandler { e ->
                if (e != null) { // Error!
                    e.printStackTrace()
                    return@OpenChannelExitHandler
                }
            })
        }
        super.onDestroy()
    }

    private fun getAvailableView(): Int {
        for( i in 0 until this.mAvailableView.size) {
            if(!mAvailableView[i]) {
                mAvailableView[i] = true
                return i
            }
        }
        return -1
    }

    fun updateViews() {
        for (i in 1..8) {
            if(!mAvailableView[i]) {
                mSurfaceViewArray[i].visibility = View.INVISIBLE
            } else {
                mSurfaceViewArray[i].visibility = View.VISIBLE
            }
        }
    }

    private fun sendUserMessage(text: String) {
        mChannel.sendUserMessage(UserMessageParams(text).setData("test"),
            BaseChannel.SendUserMessageHandler { userMessage, e ->
                if (e != null) { // Error!
                    return@SendUserMessageHandler
                }
                // Display sent message to RecyclerView
                mChatAdapter.addFirst(userMessage)
            })
    }

    private fun broadcastJoin(remonToken : String, isCaster : String){
        mChannel.sendUserMessage(MessageUtil.makeJoinMessage(remonToken,isCaster),
            BaseChannel.SendUserMessageHandler { userMessage, e ->
                if (e != null) { // Error!
                    return@SendUserMessageHandler
                }
                Log.d("devsim","boradcastJoin Success")
            }
        )
    }
}