package com.github.multistreaming

import android.app.Application
import com.sendbird.android.SendBird

class BaseApp : Application() {
    private val APP_ID = "E5548823-CF03-4C19-9B33-329CEE2B1575"

    override fun onCreate() {
        super.onCreate()
        PreferenceUtils.init(applicationContext)
        SendBird.init(APP_ID, applicationContext)
    }
}