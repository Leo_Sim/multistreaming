package com.github.multistreaming.model

data class RoomList(
    val data : List<Room>
)