package com.github.multistreaming.model

data class Room(
    val roomId : String,
    val chatChannel : String,
    val remonToken : String,
    val isCaster : String
)