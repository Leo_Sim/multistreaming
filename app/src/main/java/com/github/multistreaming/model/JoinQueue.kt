package com.github.multistreaming.model

data class JoinQueue(
    val remonToken : String,
    var isCaster : String? = null
)