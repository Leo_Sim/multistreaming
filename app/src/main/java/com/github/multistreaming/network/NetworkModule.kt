package com.github.multistreaming.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule {
    private val retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.100.68:80/").addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    val userService = retrofit.create(UserService::class.java)
}