package com.github.multistreaming.network

import com.github.multistreaming.model.Room
import com.github.multistreaming.model.RoomList
import com.google.android.gms.common.internal.safeparcel.SafeParcelable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface UserService {
    @GET("/api/joined")
    fun fetchJoinedUser(
        @Query("roomId") roomId : String
        ) : Single<Response<RoomList>>

    @GET("/api/caster")
    fun enterCaster(
        @Query("roomId") roomId : String,
        @Query("chatChannel") chatChannel : String,
        @Query("remonToken") remonToken : String
        ) : Single<Response<Any>>

    @GET("/api/user")
    fun enterUser(
        @Query("roomId") roomId : String,
        @Query("chatChannel") chatChannel : String,
        @Query("remonToken") remonToken : String
    ) : Single<Response<Any>>

    @GET("/api/left")
    fun leftUser(
        @Query("roomId") roomId : String,
        @Query("remonToken") remonToken : String
    ) : Single<Response<Any>>


}