package com.github.multistreaming

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.multistreaming.chat.ConnectionManager
import com.github.multistreaming.chat.adapter.OpenChatAdapter
import com.github.multistreaming.model.RoomList
import com.github.multistreaming.network.NetworkModule
import com.github.multistreaming.util.AbstractMessageCallback
import com.github.multistreaming.util.MessageUtil
import com.remotemonster.sdk.Config
import com.remotemonster.sdk.RemonConference
import com.remotemonster.sdk.RemonException
import com.remotemonster.sdk.RemonParticipant
import com.sendbird.android.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_join.*
import org.webrtc.SurfaceViewRenderer
import retrofit2.Call
import retrofit2.Response
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

//liveroom07
class JoinActivity : AppCompatActivity(){
    private val CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_OPEN_CHAT"
    private val CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_OPEN_CHAT"

    private var mChannelUrl: String =
        "sendbird_open_channel_741_7befa4cd8ddb194c782f937b7c4c7507a0c79bf0"

    private val CHANNEL_LIST_LIMIT = 30

    private lateinit var roomID : String
    private var mChannel: OpenChannel? = null

    private var mConference = RemonConference()
    private var mPrevMessageListQuery: PreviousMessageListQuery? = null
    private lateinit var mChatAdapter: OpenChatAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var messageCallback : AbstractMessageCallback

    private var mError: RemonException? = null

//    private lateinit var mUserViewArray: Array<Button>
    private var joinMap = HashMap<String,SurfaceViewRenderer>() //remonToken, SurfaceView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join)
        roomID = intent.getStringExtra("room")
        initMessageCallback()
        connectChat(roomID)
        initView()
//        initRemonConference()
    }

    private fun initMessageCallback(){
        messageCallback = object : AbstractMessageCallback(){
            override fun onNewUserJoined(remonToken: String, isCaster: String) {
                Log.d("devsim","ONuSER: " + remonToken + "|" + isCaster)
                if(isCaster == MessageUtil.CASTER && joinMap.containsKey(remonToken)){
                    Log.d("devsim","in")
                    val surf = joinMap[remonToken]
                    val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
                    surf?.layoutParams = params
                }
            }
        }
    }
    private fun initView(){
        send_btn.setOnClickListener { sendUserMessage(msg_edit.text.toString()) }
        setUpRecyclerView()

        layout_local.setPosition(0,66,33,33)

//         mUserViewArray = arrayOf(
//             user_me,
//             user_1,
//             user_2,
//             user_3,
//             user_4,
//             user_5,
//             user_6,
//             user_7
//            )
//
//        mUserViewArray2 = arrayOf(
//            p1,
//            p2,
//            p3,
//            p4,
//            p5,
//            p6,
//            p7,
//            p8,
//            p9
//        )
//
//        mAvailableView = Array(mUserViewArray.size) {false}
//        mAvailableView2 = Array(mUserViewArray2.size) {false}
    }

    private fun initRemonConference() {
        Log.d("devsim","twice")
        mError = null

        val config  = Config()
        config.context = this
        config.serviceId="42ad27d0-e50d-4137-8fa7-07e2f024a800"
        config.key="a63c2e56a50d7779f208fa8f23e37f64f8ee062843e8b194d973d55740df22ae"
        config.setFirstFrontFacing(true)

        mConference.create ( roomID, config ) {
            // 마스터 유저(송출자,나자신) 초기화
            Log.d("devsim","create : " + mConference.me.id)
            it.config.localView = surf_local
            it.config.remoteView = null
        }.on("onRoomCreated") {
            // 마스터 유저가 접속된 이후에 호출(실제 송출 시작)
            // TODO: 실제 유저 정보는 각 서비스에서 관리하므로, 서비스에서 채널과 실제 유저 매핑 작업 진행
            Log.d("devsim","onRoomCreated: " + it.id +"|" + mConference.me.id)
//            broadcastJoin(it.id,MessageUtil.NOT_CASTER)
        }.on("onUserJoined") {
            // 다른 사용자가 입장한 경우 초기화를 위해 호출됨
            // TODO: 실제 유저 매핑 : it.id 값으로 연결된 실제 유저를 얻습니다.
            Log.d("devsim","onUserJoined: " + it.id)
            broadcastJoin(mConference.me.id,MessageUtil.NOT_CASTER)
            val newSurf = SurfaceViewRenderer(this)
            val params = LinearLayout.LayoutParams(0,0)
            newSurf.layoutParams = params
            it.config.remoteView = newSurf
            joinMap.put(it.id,newSurf)
            Log.d("devsim","onUserJoined joinMap:")
            caster_box.addView(newSurf)
        }.on("onUserLeft") {
            val surf = joinMap[it.id]
            caster_box.removeView(surf)
            joinMap.remove(it.id)
        }
    }

    private fun connectChat(
        id : String
    ) {
        if (TextUtils.isEmpty(roomID)) {
            return
        }
        ConnectionManager.login(id, SendBird.ConnectHandler { user, e ->
            if (e != null) { // Error!
                Toast.makeText(
                    this@JoinActivity, "" + e.code + ": " + e.message,
                    Toast.LENGTH_SHORT
                )
                    .show()
                return@ConnectHandler
            }
            PreferenceUtils.setConnected(true)
            PreferenceUtils.setUserId(id)

            ConnectionManager.addConnectionManagementHandler(
                CONNECTION_HANDLER_ID
            ) { reconnect ->
                Log.d("devsim","handle1")
                refreshFirst()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        /**
         * 메시지 리시버
         */
        SendBird.addChannelHandler(
            CHANNEL_HANDLER_ID,
            object : SendBird.ChannelHandler() {
                override fun onMessageReceived(
                    baseChannel: BaseChannel,
                    baseMessage: BaseMessage
                ) { // Add new message to view
                    if (baseChannel.url == mChannelUrl) {
                        MessageUtil.unpackMessage(baseMessage,messageCallback)
//                        mChatAdapter.addFirst(baseMessage)
                    }
                }
            })
    }

    private fun setUpRecyclerView() {
        mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.setReverseLayout(true)
        mChatAdapter = OpenChatAdapter()
        chat_list.setLayoutManager(mLayoutManager)
        chat_list.setAdapter(mChatAdapter)
    }

    private fun refreshFirst() {
        if(mChannel == null) {
            enterChannel(mChannelUrl)
        }
    }

    private fun enterChannel(channelUrl: String) {
        OpenChannel.getChannel(channelUrl,
            OpenChannel.OpenChannelGetHandler { openChannel, e ->
                if (e != null) { // Error!
                    Log.d("devsim", "enterChannel err1 : " + e.message)
                    return@OpenChannelGetHandler
                }
                // Enter the channel
                openChannel.enter(OpenChannel.OpenChannelEnterHandler { e ->
                    if (e != null) { // Error!
                        Log.d("devsim", "enterChannel err2 : " + e.message)
                        return@OpenChannelEnterHandler
                    }
                    Log.d("devsim", "enterChannel success")
                    mChannel = openChannel
                    //remote
                    initRemonConference()
                    refresh()
                })
            })
    }

    private fun refresh() {
        loadInitialMessageList(CHANNEL_LIST_LIMIT)
    }

    private fun loadInitialMessageList(numMessages: Int) {
        mPrevMessageListQuery = mChannel?.createPreviousMessageListQuery()
        mPrevMessageListQuery?.load(
            numMessages,
            true,
            PreviousMessageListQuery.MessageListQueryResult { list, e ->
                if (e != null) { // Error!
                    e.printStackTrace()
                    return@MessageListQueryResult
                }
                mChatAdapter.setMessageList(list)
            })
    }


    override fun onBackPressed() {
        mConference.leave()

        super.onBackPressed()
    }

    override fun onPause() {
        ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID)
        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID)
        super.onPause()
    }

    override fun onDestroy() {
        Log.d("devsim","onDestroy")
        if (mChannel != null) {
            mChannel?.exit(OpenChannel.OpenChannelExitHandler { e ->
                if (e != null) { // Error!
                    e.printStackTrace()
                    return@OpenChannelExitHandler
                }
            })
        }
        super.onDestroy()
    }

    private fun sendUserMessage(text: String) {
        mChannel?.sendUserMessage(text,
            BaseChannel.SendUserMessageHandler { userMessage, e ->
                if (e != null) { // Error!
                    return@SendUserMessageHandler
                }
                // Display sent message to RecyclerView
                mChatAdapter.addFirst(userMessage)
            })
    }

    private fun broadcastJoin(remonToken : String, isCaster : String){
        mChannel?.sendUserMessage(
            MessageUtil.makeJoinMessage(remonToken,isCaster),
            BaseChannel.SendUserMessageHandler { userMessage, e ->
                if (e != null) { // Error!
                    return@SendUserMessageHandler
                }
                Log.d("devsim","boradcastJoin Success")
            }
        )
    }
}